/*课本p649练习题，实现将十六进制转化为点分十进制*/

#include "csapp.h"

int main(int argc, char **argv)
{
	struct in_addr inaddr;
	uint32_t addr;
	char buf[MAXBUF];
	
	if(argc!=2){
		fprintf(stderr, "usage: %s <hex number>\n", argv[0]);
		exit(0);
	}
	scanf(argv[1], "%x", &addr);
	inaddr.s_addr = htonl(addr);

	if(!inet_ntop(AF_INET, &inaddr, buf, MAXBUF))
		unix_error("inet_ntop");
	printf("%s\n", buf);

	exit(0);
}
