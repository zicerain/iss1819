#include "csapp.h"
#include <sys/types.h>
#include <stdlib.h>
#define N 2

int main()
{
	int status, i;
	pid_t pid;

	/*Parent creates N children*/
	for(i=0; i<N; i++)
		if((pid = fork()) == 0) /*Child*/
			exit(100+i);

	/*Parent reaps N children in no particular order*/
	while ((pid = waitpid(-1, &status, 0))>0){	//pid=-1,等待集合由父进程的所有子进程组成
		if(WIFEXITED(status))
			printf("child %d terminated normally with exit status=%d\n", pid, WEXITSTATUS(status));
	else
		printf("child %d terminated abnormally\n", pid);
	}

	/*The only normal termination is if there are no more children*/
	if (errno != ECHILD)
		unix_error("waitpid error");

	exit(0);
}

